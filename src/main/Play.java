package main;

import java.util.*;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.*;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.*;
import org.newdawn.slick.fills.GradientFill;

import initializations.*;
import movables.Player;
import movables.Zombie;
import movables.Bullet;
import movables.Movable;

public class Play extends BasicGameState {
	/*
	 * A state that handles everything related to the main gameplay
	 */
	
	public final int id;
	private Image background;
	private Image configIcon;
	private DialogBox configDBox; // Play Config Dialog Box
	private DialogBox gameOverDBox; // Play Game Over Dialog Box
	
	private Input gameStageInput; // To store any user input
	private TextField inputBar; // The main text field where the user types in
	private String currentText = ""; // Current user-typed text
	
	private Player player;
	private ArrayList<Zombie> zombies; // List of zombies
	private ArrayList<Bullet> bullets; // List of bullets
	private float score; // Player's score
	private float zombieAcceleration = 0.02f;
	private float zombieSavedSpeed;
	private boolean showMenu;
	
	// Music and sounds
	private Music bgMusic;
	private Sound shootSound;
	private Sound[] hitSounds;
	private int hitSoundIndex;
	private Sound dieSound;
	
	// A dictionary for storing words and their damage
	private HashMap<String, Float> wordDmgMap;
	
	public Play(int id) {
		this.id = id;
	}

	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		// Download all the words based on difficulty
		Database db = new Database();
		wordDmgMap = db.retrieveWords(Game.diff);
		
		// initialize background
		background = new Image("res/images/background/bg_dessert_0.png");
		background = background.getScaledCopy(gc.getWidth(), gc.getHeight());
		
		// initialize score
		score = 0;
		
		showMenu = false;
		
		// initialize configuration icon
		configIcon = new Image("res/images/decoration/configure.png");
		
		// initialize config dialog box
		configDBox = new DialogBox("Menu", "Do you want to end?", "Resume", "Main menu", "");
		
		// initialize game over dialog box
		gameOverDBox = new DialogBox("Game Over", "Keep trying, our hero!", "Restart", "Main menu", "");
	
		// Get user input
		gameStageInput = gc.getInput();
				
		// initialize inputBar
		inputBar = new TextField(gc, gc.getDefaultFont(), gc.getWidth()/4, gc.getHeight() -25, 300, 25);
		inputBar.setInput(gameStageInput);
		
		// initialize player
		player = Resources.createPlayer("Victor");
		
		// initialize zombies
		zombies = Engine.produceZombies(wordDmgMap, Game.diff, 0.0f);
		
		// initialize bullets list
		bullets = new ArrayList<Bullet>();
		
		// initialize music and sounds
		bgMusic = new Music("res/musics/Intense_Theme.ogg");
		shootSound = new Sound("res/sounds/8bit_gunloop_explosion.ogg");
		hitSounds = new Sound[5];
		for (int i = 1; i <= hitSounds.length; i++) {
			hitSounds[i - 1] = new Sound("res/sounds/hit_sounds/ogg/hit" + i + ".ogg");
		}
		hitSoundIndex = 0;
		dieSound = new Sound("res/sounds/hit_sounds/ogg/die1.ogg");
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		
		// render background
		background.draw(0,0);
		
		// render inputBar
		g.setColor(Color.white);
		inputBar.render(gc, g);
		
		// render the configuration icon
		configIcon.draw(0, 600 - configIcon.getHeight());
		
		// render player
		if (player.current == player.shootingPistol){
			if (!shootSound.playing())
				shootSound.play();
			player.current.stopAt(player.current.getFrameCount() - 1);
			player.current.draw(player.minX, player.minY);
			player.shootEffect.draw(player.minX + 70, player.minY + 10);
			if (player.current.isStopped()) {
				player.current.restart();
				player.current = player.shootingIdle;
			}
		}
		else  {
			player.current.draw(player.minX, player.minY);		
		}
		
		g.setColor(Color.black);
		
		// render each zombie if it is not dead
		for (Zombie zombie: zombies) {
			if(zombie.direction == Movable.Direction.RIGHT){
				// this is when the zombie bounces
				zombie.current.stopAt(zombie.current.getFrameCount()-1);
				zombie.current.draw(zombie.minX, zombie.minY);
				g.drawString(zombie.word, zombie.minX + 15, zombie.minY - 10);

				if(zombie.current.isStopped()){
					zombie.current.restart();
					zombie.direction = Movable.Direction.LEFT;
					zombie.current = zombie.movingLeft;
				}
			
			}
			else {
				zombie.current.draw(zombie.minX, zombie.minY);
				g.drawString(zombie.word, zombie.minX + 15, zombie.minY - 10);
			}
		}
		
		// Draw player's health bar
		g.drawString("HP", 25, 40);
		g.fill(player.healthBar, new GradientFill(0, 0, Color.yellow, 0,100, Color.red) );
		g.drawString((int)player.HP + "/" + (int)player.maxHP, player.healthBar.getMinX(), player.healthBar.getMaxY() + 10);
		
		// render bullet if it exists
		for (Bullet bullet: bullets) {
			bullet.current.draw(bullet.minX, bullet.minY);
		}
		
		// Show score
		g.drawString("Score: " + score, 600, 40);
		
		// Save the speed of zombies for pausing purpose
		if (!zombies.isEmpty()) {
			if (zombies.get(0).speed > 0) {
				zombieSavedSpeed = zombies.get(0).speed;
			}
		}
		
		// Show menu
		if(showMenu){
			// Pause all the zombies
			for (Zombie zombie: zombies){
				zombie.stopMovement();
			}
			configDBox.draw(g);
			
			// If resume button clicked
			if (configDBox.isButton1Clicked()) {
				showMenu = false;
				
				// Move the zombies
				for (Zombie zombie: zombies){
					zombie.current = zombie.movingLeft;
					zombie.startMovement(Movable.Direction.LEFT, zombieSavedSpeed);
				}
			} else if (configDBox.isButton2Clicked()) {
				// If main menu button clicked
				showMenu = false;
				sbg.enterState(Game.MENU);
			}
		}
		
		if (player.HP <= 0) {
			// If player dies
			
			if (score > Game.playerHighscore) {
				// Set highscore
				Game.playerHighscore = score;
				FileProcessor.writeHighscore();
			}
			
			if (!player.hasDied) {
				// Play dying sound
				if (!dieSound.playing()) {
					dieSound.play();
					player.hasDied = true;
				}
			}
			
			// Stop all the zombies
			for (Zombie zombie: zombies){
				zombie.stopMovement();
			}	
			
			// Draw gameOverDBox on screen
			gameOverDBox.titlePosX = gameOverDBox.xPos + 60;
			gameOverDBox.draw(g);
			
			if (gameOverDBox.isButton1Clicked()) {
				// Restart the playLAN
				this.init(gc, sbg);
			} else if (gameOverDBox.isButton2Clicked()) {
				// Back to main menu
				sbg.enterState(Game.MENU);
			}
		}
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		// Play background music
		if(!bgMusic.playing())
			bgMusic.loop();
		
		inputBar.setFocus(true);
		
		// If no zombie is left, produce more zombies with accelerated speed
		if (zombies.isEmpty()) {
			zombies = Engine.produceZombies(wordDmgMap, Game.diff, zombieAcceleration);
		}

		// Launch bullet if the words match
		if(gameStageInput.isKeyPressed(Input.KEY_SPACE)){
			currentText = inputBar.getText().trim();
			for (Zombie zombie: zombies) {

				if (currentText.equals(zombie.word)) {
					// Change player current animation
					player.current = player.shootingPistol;
					// Create new bullet and add it to the list
					bullets.add(Engine.produceBullet(wordDmgMap, player, zombie));
					currentText = ""; // Reset current text
				}

			}
			inputBar.setText(""); // Empty the text bar
		}
		
		
		Bullet[] bulletsArray = bullets.toArray(new Bullet[bullets.size()]);
		Zombie[] zombiesArray = zombies.toArray(new Zombie[zombies.size()]);
		for (Bullet bullet: bulletsArray) {
			// Move the bullet if it exists and it is moving
			if (bullet.isMoving)
				bullet.move(bullet.speed * delta);
			
			for (Zombie zombie: zombiesArray) {
				// Damage the target zombie if the bullet collides with it
				
				if (bullet.targetZombie != zombie)
					continue;
				if (Engine.isCollision(bullet, zombie)) {
					zombie.HP -= bullet.damage; // Damage the zombie
					bullets.remove(bullet);
					if (zombie.HP <= 0)
						zombies.remove(zombie);
					score += 50 + bullet.damage; // Add score
				}
			}
		}
		
		// if the player and zombie collides 
		for (Zombie zombie: zombiesArray){
			if (Engine.isCollision(player, zombie)){
				// Play hit sounds
				if (!hitSounds[hitSoundIndex].playing()) {
					hitSounds[hitSoundIndex].play();
					hitSoundIndex++;
					if (hitSoundIndex >= hitSounds.length) {
						hitSoundIndex = 0;
					}
				}
				player.takeDamage(zombie.damage); // Damage player
				zombie.bounce(); // The zombies move several steps back
			}
			
		}
		
		// Move the zombie if it is moving
		for (Zombie zombie: zombies) {
				if (zombie.isMoving)
					zombie.move(zombie.speed * delta);
		}
		
		// if the user presses the gear button or escape button, show menu
		if (gameStageInput.isKeyDown(Input.KEY_ESCAPE) || (Engine.isClicked(0, 0, 40, 40) && Mouse.isButtonDown(0)))
			showMenu = true;
		
	}
	
	@Override
	public int getID() {
		return id;
	}
}
