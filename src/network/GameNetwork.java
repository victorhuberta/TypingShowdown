package network;

import java.io.*;
import java.net.*;

import main.PlayLAN;
import movables.SerializablePlayer;

public class GameNetwork implements Runnable {
	/*
	 * A game network based on UDP, it sends and receives
	 * GameInfo objects to update the necessary information
	 * in the PlayLAN state
	 */
	public PlayerInfo pInfo;
	public GameInfo gInfo;
	public boolean isTimeout = false;
	public float damage;
	public int port = 10000;
	
	DatagramSocket socket = null;
	ByteArrayOutputStream baos = null;
	ObjectOutputStream oos = null;
	ByteArrayInputStream bais = null;
	ObjectInputStream ois = null;
	
	public GameNetwork(PlayerInfo pInfo) {
		this.pInfo = pInfo;
	}
	
	@Override
	public void run() {
		while (true) {
			/*
			 * If not timeout, the damage is not reset.
			 * This is a workaround for the occasional packet lost
			 */
			if (!isTimeout) {
				damage = 0;
			}
			
			// Keep trying to create socket if the address is in use
			while (socket == null || socket.isClosed()) {
				try {
					socket = new DatagramSocket(port);
				} catch (SocketException e) {
					System.out.println(e.getMessage());
					port++;
					System.out.println("Trying another port...");
				}
			}
			
			// Exchange GameInfo objects between two players
			exchangeGameInfo();
			
			// Set the GameInfo in PlayLAN state
			PlayLAN.gInfo = gInfo;
			
			if (Thread.interrupted()) {
				stop(); // close the socket
			}
		}
	}
	
	public void stop() {
		// close the socket
		if (socket != null) {
			socket.close();
		}
	}
	
	public void exchangeGameInfo() {
		/*
		 * Send and receive GameInfo objects
		 */
		try {
			// Set socket timeout to 2 seconds
			socket.setSoTimeout(2000);
			byte[] data; // buffer used by the socket
			
			baos = new ByteArrayOutputStream();
			oos = new ObjectOutputStream(baos);
			
			// Set GameInfo variables
			boolean gameOver = PlayLAN.gameOver;
			SerializablePlayer sPlayer = new SerializablePlayer(PlayLAN.player);
			damage += PlayLAN.playerDamage;
			
			// Reset playerDamage to 0
			PlayLAN.playerDamage = 0;
			
			// Write a new GameInfo object to ObjectOutputStream 
			oos.writeObject(new GameInfo(gameOver, sPlayer, damage));
			
			data = baos.toByteArray();
			DatagramPacket packet = new DatagramPacket(data, data.length, pInfo.address, port);
			
			// Send packet to specified address
			socket.send(packet);
			
			data = new byte[data.length + 1024]; // Increase buffer size to cater new data
			packet = new DatagramPacket(data, data.length);
			
			// Receive packet
			socket.receive(packet);
			
			bais = new ByteArrayInputStream(packet.getData());
			ois = new ObjectInputStream(bais);
			
			try {
				// Read GameInfo object
				gInfo = (GameInfo) ois.readObject();
			} catch (ClassNotFoundException e) {
				System.out.println(e.getMessage());
			} catch (EOFException e) {
				e.printStackTrace();
			}
			
			// No timeout if the code reach this point
			isTimeout = false;
		
		} catch (SocketTimeoutException e) {
			isTimeout = true;
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			// Close all input and output streams
			try {
				oos.close();
				baos.close();
				if (ois != null) {
					ois.close();
				}
				bais.close();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (NullPointerException e) {
				System.out.println(e.getMessage());
			}
		}
	}
}
