package movables;

public class Zombie extends Movable {
	public String word;
	
	public Zombie() {}
	
	public Zombie(String word) {
		super();
		this.word = word;
	}
	
	public void bounce(){
		/*
		 * Zombie bounces if it hits player
		 */
		this.direction = Movable.Direction.RIGHT;
		this.speed = 0.03f;
	}
	
}
