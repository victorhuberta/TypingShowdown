package main;

import org.newdawn.slick.state.StateBasedGame;
import org.newdawn.slick.state.BasicGameState;

import org.newdawn.slick.SlickException;
import org.newdawn.slick.Image;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.GameContainer;
import org.newdawn.slick.Music;

public class Splash extends BasicGameState {
	/*
	 * Splash screen state
	 */
	
	private int stateID;
	private Image pegi; // pegi 12 image
	private final int ELAPSED = 2000;
	private int duration = 0;
	private boolean fadeOut = false;
	private Music bgMusic;
	boolean played = true;
	
	public Splash(int ID){
		this.stateID = ID;
	}

	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException{
		// initialize image
		pegi = new Image("res/images/splash/pegi12.png");
		pegi.setAlpha(0);
		
		// initialize music
		bgMusic = new Music("res/sounds/pegi_12.ogg");
	}
	
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException{
		pegi.drawCentered(Game.width/2, Game.height/2);
	}
	
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException{ 
		
		if(played){
			// play music
			if(!bgMusic.playing()){
				bgMusic.play();
				played = false;
			}
		}
		
		if(fadeOut){
			duration -= 3;
			pegi.setAlpha(pegi.getAlpha() - 0.005f);
		}
		if(duration < ELAPSED && !fadeOut){
			pegi.setAlpha(pegi.getAlpha() + 0.01f);
			duration += delta;
		} 
		else if (duration >= ELAPSED){
			fadeOut = true;
		}
		else if (duration < 0){
			sbg.enterState(Game.MENU);
		}
			
	}
	
	public int getID(){
		return stateID;
	}	
}
