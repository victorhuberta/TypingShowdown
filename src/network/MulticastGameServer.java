package network;

import java.io.IOException;
import java.net.*;
import java.util.*;

public class MulticastGameServer implements Runnable {
	/*
	 * Discover other players in LAN, and add their information
	 * into a TreeSet
	 */
	
	private MulticastSocket socket;
	private InetAddress localIPv4Address;
	private InetAddress group;
	private int port = 9999;
	
	private final long DELAY = 1000;
	
	public static TreeSet<PlayerInfo> otherPlayers = new TreeSet<>();
	
	public void run() {
		/*
		 * Search other players in LAN with 1 second delay
		 */
		try {
			while (true) {
				while (socket == null) {
					try {
						socket = new MulticastSocket(port);
					} catch (IOException e) {
						System.out.println(e.getMessage());
					}
				}
				searchPlayers(false);
			    Thread.sleep((long)(Math.random() * DELAY));
			}
		} catch (InterruptedException e) {
			stop();
		}
		
	}
	
	public void stop() {
		if (socket != null) {
			socket.close();
		}
	}
	
	public void searchPlayers(boolean reset) {
		/*
		 * Search other players in the LAN by receiving multi-cast packets
		 */
		try {
			// If reset is set to true, clear the TreeSet
			if (reset) {
				synchronized (this){
					MulticastGameServer.otherPlayers.clear();
				}
				
				if (socket == null) {
					socket = new MulticastSocket(port);
				}
			}
			localIPv4Address = getFirstNonLoopbackAddress(true, false);
			group = InetAddress.getByName("239.255.99.99");
			socket.joinGroup(group); // Join multi-cast group
			
			// Receive packet from the multi-cast group
			byte[] data = new byte[1024];
			DatagramPacket packet = new DatagramPacket(data, data.length);
			socket.receive(packet);
			
			// Add PlayerInfo only if the packet is not from this machine
			if (!localIPv4Address.toString().equals(packet.getAddress().toString())) {
				
				// Process the received packet and turn it into a PlayerInfo object
				String received = new String(packet.getData(), 0, packet.getLength());
				String[] playerDetails = received.split(":");
				String playerName = playerDetails[0];
				double playerHighScore = Double.parseDouble(playerDetails[1]);
				
				PlayerInfo pInfo = new PlayerInfo(playerName, playerHighScore,
						packet.getAddress(), packet.getPort());
				
				// Add PlayerInfo into otherPlayers TreeSet
				MulticastGameServer.otherPlayers.add(pInfo);
			}
			
			socket.leaveGroup(group); // Leave multi-cast group
			if (reset && socket != null) {
				socket.close();
			}
		} catch (IOException e) {
			System.out.println("ERROR: " + e.getMessage());
		}
	}
	
	private InetAddress getFirstNonLoopbackAddress(boolean preferIpv4, boolean preferIPv6) throws SocketException {
		/*
		 * Get first local non-loopback address, which depends on IP preference
		 * Ex: 192.168.0.5, 192.168.0.8, 10.11.23.56, etc...
		 */
	    Enumeration<NetworkInterface> en = NetworkInterface.getNetworkInterfaces();
	    while (en.hasMoreElements()) {
	        NetworkInterface i = (NetworkInterface) en.nextElement();
	        // Get IP addresses from every interface
	        for (Enumeration<InetAddress> en2 = i.getInetAddresses(); en2.hasMoreElements();) {
	            InetAddress addr = (InetAddress) en2.nextElement();
	            
	            // Check for non-loopback addresses
	            if (!addr.isLoopbackAddress()) {
	            	// If preference is IPv4
	                if (addr instanceof Inet4Address) {
	                    if (preferIPv6) {
	                        continue;
	                    }
	                    return addr;
	                }
	             // If preference is IPv6
	                if (addr instanceof Inet6Address) {
	                    if (preferIpv4) {
	                        continue;
	                    }
	                    return addr;
	                }
	            }
	        }
	    }
	    return null;
	}
}
