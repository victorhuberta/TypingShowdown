package movables;

import java.io.Serializable;
import movables.Movable.Direction;

public class SerializableMovable<T extends Movable> implements Serializable {
	
	/*
	 * A Movable class which is serializable (can be sent within network)
	 */
	
	private static final long serialVersionUID = 1L;

	public enum Animation {
		idle, movingUp, movingDown, movingLeft, movingRight
	}
	
	public float HP, damage;
	public Animation current;
	public float minX, minY, maxX, maxY;
	public boolean isMoving = false;
	public Direction direction = Direction.RIGHT;
	public float speed = 0.0f;
	
	public SerializableMovable(T movable) {
		/*
		 * Construct a serializable Movable object by manipulating an existing Movable object
		 */
		
		this.HP = movable.HP;
		this.damage = movable.damage;
		
		// Sending Animation object within network is storage-heavy, 
		// so it converts the Animation objects to the corresponding Enums.
		if (movable.current == movable.idle) {
			this.current = Animation.idle;
		} else if (movable.current == movable.movingUp) {
			this.current = Animation.movingUp;
		} else if (movable.current == movable.movingDown) {
			this.current = Animation.movingDown;
		} else if (movable.current == movable.movingLeft) {
			this.current = Animation.movingLeft;
		} else if (movable.current == movable.movingRight) {
			this.current = Animation.movingRight;
		}
		
		// Set location and behaviour of the Movable object
		this.minX = movable.minX;
		this.minY = movable.minY;
		this.maxX = movable.maxX;
		this.maxY = movable.maxY;
		this.isMoving = movable.isMoving;
		this.direction = movable.direction;
	}
	
	protected T deserialize(T movable) {
		/*
		 * Return a new Movable object based on this object
		 */
		
		if (movable == null) {
			return null;
		}
		
		movable.HP = this.HP;
		movable.damage = this.damage;
		movable.minX = this.minX;
		movable.minY = this.minY;
		movable.maxX = this.maxX;
		movable.maxY = this.maxY;
		movable.isMoving = this.isMoving;
		movable.direction = this.direction;
		movable.speed = this.speed;
		
		return movable;
	}
}
