package initializations;

import org.newdawn.slick.*;
import org.newdawn.slick.geom.Rectangle;

import java.util.Arrays;
import main.Game;

import movables.*;

public class Resources {
	/*
	 * Initializes all the resources needed in the game
	 */
	
	public static Player createPlayer(String name) throws SlickException {
		/*
		 * The initialization of Player object
		 */
		
		// Constants required for the animation 
		// i.e. frames count and duration for each frame
		final int IDLE = 1;
		final int[] IDLE_DURATION = new int[IDLE];
		Arrays.fill(IDLE_DURATION, 50);
		
		final int SHOOTING_IDLE = 1;
		final int[] SHOOTING_IDLE_DURATION = new int[SHOOTING_IDLE];
		Arrays.fill(SHOOTING_IDLE_DURATION, 100);
		
		final int SHOOTING_PISTOL = 3;
		final int[] SHOOTING_PISTOL_DURATION = new int[SHOOTING_PISTOL];
		Arrays.fill(SHOOTING_PISTOL_DURATION, 100);
		
		final int WALKING_NO_WEAPON = 8;
		final int[] WALKING_NO_WEAPON_DURATION = new int[WALKING_NO_WEAPON];
		Arrays.fill(WALKING_NO_WEAPON_DURATION, 200);
		
		final int WALKING_PISTOL = 8;
		final int[] WALKING_PISTOL_DURATION = new int[WALKING_PISTOL];
		Arrays.fill(WALKING_PISTOL_DURATION, 200);
		
		final int SHOOT_EFFECT = 14;
		final int[] SHOOT_EFFECT_DURATION = new int[SHOOT_EFFECT];
		Arrays.fill(SHOOT_EFFECT_DURATION, 100);
		
		// Create frames from static resources
		Image[] idleFrames = new Image[IDLE];
		for (int i = 0; i < IDLE; i++) {
			idleFrames[i] = new Image("res/images/player/idle/player-idle-" + i + ".png");
		}
		
		Image[] shootingIdleFrames = new Image[SHOOTING_IDLE];
		for (int i = 0; i < SHOOTING_IDLE; i++) {
			shootingIdleFrames[i] = new Image("res/images/player/fire-standing/idle/player-fire-0-0" + i + ".png");
		}
		
		Image[] shootingPistolFrames = new Image[SHOOTING_PISTOL];
		for (int i = 0; i < SHOOTING_PISTOL; i++) {
			shootingPistolFrames[i] = new Image("res/images/player/fire-standing/pistol/player-fire-1-0" + i + ".png");
		}
		
		Image[] walkingNWFrames = new Image[WALKING_NO_WEAPON];
		for (int i = 0; i < WALKING_NO_WEAPON; i++) {
			walkingNWFrames[i] = new Image("res/images/player/walking/no-weapon/player-walk-0-0" + i + ".png");
		}
		
		Image[] walkingPistolFrames = new Image[WALKING_PISTOL];
		for (int i = 0; i < WALKING_PISTOL; i++) {
			walkingPistolFrames[i] = new Image("res/images/player/walking/pistol/player-walk-1-0" + i + ".png");
		}
		
		Image[] shootEffectFrames = new Image[SHOOT_EFFECT];
		for (int i = 0; i < SHOOT_EFFECT; i++) {
			shootEffectFrames[i] = new Image("res/images/effect/shoot_explosion" + i + ".png");
		}
		
		// Create the Player object and health bar
		Player player = new Player(name, new Rectangle(65 ,30, 250, 30), 100);
		// player.HP = 25;
		player.damage = 0;
		player.setIdle(idleFrames, IDLE_DURATION);
		player.setMovingRight(walkingNWFrames, WALKING_NO_WEAPON_DURATION);
		player.setShootingIdle(shootingIdleFrames, SHOOTING_IDLE_DURATION);
		player.setShootingPistol(shootingPistolFrames, SHOOTING_PISTOL_DURATION);
		player.setMovingRightPistol(walkingPistolFrames, WALKING_PISTOL_DURATION);
		player.setShootEffect(shootEffectFrames, SHOOT_EFFECT_DURATION);
		player.current = player.idle;
		float minX = (float) (0.2 * Game.width);
		float minY = (float) (0.65 * Game.height);
		player.setXY(minX, minY);
		return player;
	}
	
	public static Player createOpponent(String name) throws SlickException {
		/*
		 * The initialization of Opponent object
		 */
		
		// Constants required for the animation 
		// i.e. frames count and duration for each frame
		final int IDLE = 4;
		final int[] IDLE_DURATION = new int[IDLE];
		Arrays.fill(IDLE_DURATION, 200);
		
		final int SHOOTING_IDLE = 4;
		final int[] SHOOTING_IDLE_DURATION = new int[SHOOTING_IDLE];
		Arrays.fill(SHOOTING_IDLE_DURATION, 200);
		
		final int SHOOTING_PISTOL = 4;
		final int[] SHOOTING_PISTOL_DURATION = new int[SHOOTING_PISTOL];
		Arrays.fill(SHOOTING_PISTOL_DURATION, 200);
		
		final int WALKING_NO_WEAPON = 4;
		final int[] WALKING_NO_WEAPON_DURATION = new int[WALKING_NO_WEAPON];
		Arrays.fill(WALKING_NO_WEAPON_DURATION, 200);
		
		final int WALKING_PISTOL = 4;
		final int[] WALKING_PISTOL_DURATION = new int[WALKING_PISTOL];
		Arrays.fill(WALKING_PISTOL_DURATION, 200);
		
		final int SHOOT_EFFECT = 14;
		final int[] SHOOT_EFFECT_DURATION = new int[SHOOT_EFFECT];
		Arrays.fill(SHOOT_EFFECT_DURATION, 100);
		
		// Create frames from static resources
		Image[] idleFrames = new Image[IDLE];
		for (int i = 0; i < IDLE; i++) {
			idleFrames[i] = new Image("res/images/cowboy/idle_without_gun/cowboy4_idle_without_gun_" + i + ".png");
			idleFrames[i] = idleFrames[i].getScaledCopy(100, 100);
		}
		
		Image[] shootingIdleFrames = new Image[SHOOTING_IDLE];
		for (int i = 0; i < SHOOTING_IDLE; i++) {
			shootingIdleFrames[i] = new Image("res/images/cowboy/idle_with_gun/cowboy4_idle_with_gun_" + i + ".png");
			shootingIdleFrames[i] = shootingIdleFrames[i].getScaledCopy(100, 100);
		}
		
		Image[] shootingPistolFrames = new Image[SHOOTING_PISTOL];
		for (int i = 0; i < SHOOTING_PISTOL; i++) {
			shootingPistolFrames[i] = new Image("res/images/cowboy/shoot/cowboy4_shoot_" + i + ".png");
			shootingPistolFrames[i] = shootingPistolFrames[i].getScaledCopy(100, 100);
		}
		
		Image[] walkingNWFrames = new Image[WALKING_NO_WEAPON];
		for (int i = 0; i < WALKING_NO_WEAPON; i++) {
			walkingNWFrames[i] = new Image("res/images/cowboy/walk_without_gun/cowboy4_walk_without_gun_" + i + ".png");
			walkingNWFrames[i] = walkingNWFrames[i].getScaledCopy(100, 100);
		}
		
		Image[] walkingPistolFrames = new Image[WALKING_PISTOL];
		for (int i = 0; i < WALKING_PISTOL; i++) {
			walkingPistolFrames[i] = new Image("res/images/cowboy/walk_with_gun/cowboy4_walk_with_gun_" + i + ".png");
			walkingPistolFrames[i] = walkingPistolFrames[i].getScaledCopy(100, 100);
		}
		
		Image[] shootEffectFrames = new Image[SHOOT_EFFECT];
		for (int i = 0; i < SHOOT_EFFECT; i++) {
			shootEffectFrames[i] = new Image("res/images/effect/shoot_explosion" + i + ".png");
		}
		
		// Create the Player object
		Player opponent = new Player(name, new Rectangle(Game.width - 65 ,30, -250, 30), 100);
		// opponent.HP = 25;
		opponent.damage = 0;
		opponent.setIdle(idleFrames, IDLE_DURATION);
		opponent.setMovingRight(walkingNWFrames, WALKING_NO_WEAPON_DURATION);
		opponent.setShootingIdle(shootingIdleFrames, SHOOTING_IDLE_DURATION);
		opponent.setShootingPistol(shootingPistolFrames, SHOOTING_PISTOL_DURATION);
		opponent.setMovingLeftPistol(walkingPistolFrames, WALKING_PISTOL_DURATION);
		opponent.setShootEffect(shootEffectFrames, SHOOT_EFFECT_DURATION);
		opponent.current = opponent.idle;
		float minX = (float) (0.7 * Game.width);
		float minY = (float) (0.62 * Game.height);
		opponent.setXY(minX, minY);
		return opponent;
	}

	public static Zombie createZombie(String word) throws SlickException {
		/*
		 * The initialization of Zombie object
		 */
		
		// Constants required for the animation 
		// i.e. frames count and duration for each frame
		final int IDLE = 2;
		final int[] IDLE_DURATION = new int[IDLE];
		Arrays.fill(IDLE_DURATION, 600);
		
		final int MOVING_LEFT = 6;
		final int[] MOVING_LEFT_DURATION = new int[MOVING_LEFT];
		Arrays.fill(MOVING_LEFT_DURATION, 200);
		
		// Create frames from static resources
		Image[] idleFrames = new Image[IDLE];
		for (int i = 0; i < IDLE; i++) {
			idleFrames[i] = new Image("res/images/zombie-a/zombie-a-0-0"+i+".png");
		}
		
		Image[] movingLeftFrames = new Image[MOVING_LEFT];
		for (int i = 0; i < MOVING_LEFT; i++) {
			movingLeftFrames[i] = new Image("res/images/zombie-a/zombie-a-0-0" + i + ".png");
		}
		
		// Create a Movable object named zombie
		Zombie zombie = new Zombie(word);
		zombie.HP = 1;
		zombie.damage = 3;
		zombie.setIdle(idleFrames, IDLE_DURATION);
		zombie.setMovingLeft(movingLeftFrames , MOVING_LEFT_DURATION);
		zombie.current = zombie.movingLeft;
		float minX = (float) (0.8 * Game.width);
		float minY = (float) (0.65 * Game.height);
		zombie.setXY(minX, minY);
		
		return zombie;
	}
	
	public static Bullet createBullet(float x, float y, Movable target, float damage) throws SlickException {
		/*
		 * The initialization of Zombie object
		 */
		
		// Constants required for the animation 
		// i.e. frames count and duration for each frame
		final int IDLE = 2;
		final int[] IDLE_DURATION = new int[IDLE];
		Arrays.fill(IDLE_DURATION, 200);
		
		final int MOVING_RIGHT = 2;
		final int[] MOVING_RIGHT_DURATION = new int[MOVING_RIGHT];
		Arrays.fill(MOVING_RIGHT_DURATION, 200);
		
		// Create frames from static resources
		Image[] idleFrames = new Image[IDLE];
		for (int i = 0; i < IDLE; i++) {
			idleFrames[i] = new Image("res/images/ammo/bullet-gun.png");
		}
		
		Image[] movingRightFrames = new Image[MOVING_RIGHT];
		for (int i = 0; i < MOVING_RIGHT; i++) {
			movingRightFrames[i] = new Image("res/images/ammo/bullet-gun.png");
		}

		// Create a Bullet object
		Bullet bullet = new Bullet();
		if (target instanceof Zombie)
			bullet = new Bullet((Zombie) target);
		
		bullet.HP = 1;
		bullet.damage = damage;
		bullet.setIdle(idleFrames, IDLE_DURATION);
		bullet.setMovingRight(movingRightFrames, MOVING_RIGHT_DURATION);
		bullet.current = bullet.movingRight;
		float minX = x;
		float minY = y;
		bullet.setXY(minX, minY);
		
		return bullet;
	}
	
}
