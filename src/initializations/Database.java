package initializations;

import java.sql.*;

import java.util.HashMap;

import main.Game;


public class Database {
	/*
	 * Handles everything related to database
	 */
	private final String username = "appUser";
	private final String password = "appPassword";
	private final String serverName = "localhost";
	private int portNumber = 3306;
	private final String dbName = "TypingShowdown";
	private final String tableName = "words";
	
	public Connection getConnection() throws SQLException {
		/*
		 * Getting a connection with the driver
		 */
		Connection conn = null;
		
		conn = DriverManager.getConnection("jdbc:mysql://"
				+ this.serverName + ":" + this.portNumber + "/" 
				+ this.dbName, username, password);
		
		return conn;
	}
	
	public boolean executeUpdate(Connection conn, String command) throws SQLException {
		/*
		 * Execute update query to the database
		 */
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(command);
			return true;
		} finally {
			if (stmt != null) { stmt.close(); }
		}
	}
	
	public HashMap<String, Float> retrieveWords(Game.Difficulty diff) {
		/*
		 * Retrieve a word list with the specified difficulty,
		 * and calculate the damage along the way
		 */
		Statement stmt = null;
		
		// The query select all the words and their char_lengths
		String query = 
				"SELECT word, CHAR_LENGTH(word) AS 'length' FROM "
				+ tableName + " WHERE difficulty = LOWER('" + diff.toString() + "');";
		
		// A dictionary to store the mappings of words and damage
		HashMap<String, Float> wordDmgMap = new HashMap<String, Float>();
		try {
			Connection conn = getConnection();
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			// For each row in result set, calculate the damage
			// and put the mapping into wordDmgMap
			while (rs.next()) {
				float length = (float) rs.getInt("length");
				float damage = length / diff.getNumVal();
				wordDmgMap.put(rs.getString("word").trim(), damage);
			}
			stmt.close();
			conn.close();
			
		} catch (SQLException e) {
			System.out.println("ERROR: " + e.getMessage());
			
			/*
			 * Try port 3307 if port 3306 didn't work.
			 * If it isn't working, maybe MySQL server hasn't been started.
			 */
			if (portNumber == 3306) {
				System.out.println("Trying port 3307...");
				portNumber = 3307;
				return this.retrieveWords(diff);
			} else {
				System.out.println("Have you started the MySQL server yet?");
			}
			System.exit(1);
		}
		
		
		return wordDmgMap;
	}
}
