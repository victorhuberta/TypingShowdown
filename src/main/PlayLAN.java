package main;

import java.util.*;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.*;
import org.newdawn.slick.fills.GradientFill;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.*;

import initializations.*;
import movables.*;
import network.*;

public class PlayLAN extends BasicGameState {
	/*
	 * A state that handles everything related to the main PvP gameplay
	 */
	public final int id;
	private Image background;
	public static GameInfo gInfo;
	private Image configIcon;
	private DialogBox configDBox; // PlayLAN Config Dialog Box
	private boolean showMenu;
	
	// Previous unique value of GameInfo
	public static long previousRandomValue;
	
	// True if this player loses
	public static boolean lose;
	
	// True if one of the player loses
	public static boolean gameOver;
	
	private DialogBox gameOverDBox;
	
	private Input gameStageInput; // To store any user input
	private TextField inputBar; // The main text field where the user types in
	private String currentText; // Current user-typed text
	
	public static ArrayList<Word> wordsList; // To store all words
	
	// A dictionary for storing words and their damage
	private HashMap<String, Float> wordDmgMap;
	
	public static Player player;
	public static Player opponent;
	public static float playerDamage;
	
	// Music and sounds
	private Music bgMusic;
	private Sound shootSound;
	private Sound[] hitSounds;
	private int hitSoundIndex;
	private Sound dieSound;
	
	public PlayLAN(int id) {
		this.id = id;
	}

	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		// Download all the words based on difficulty
		Database db = new Database();
		wordDmgMap = db.retrieveWords(Game.diff);
		
		// initialize gameInfo and its unique value
		gInfo = null;
		previousRandomValue = -1;
		
		// initialize background
		background = new Image("res/images/background/bg_dessert_0.png");
		background = background.getScaledCopy(gc.getWidth(), gc.getHeight());
		
		// initialize configuration icon
		configIcon = new Image("res/images/decoration/configure.png");
		
		// initialize config dialog box
		configDBox = new DialogBox("P vs P", "Do you want to end?", "Resume", "Disconnect", "");
		
		showMenu = false;
		
		// Get user input
		gameStageInput = gc.getInput();
		
		// initialize words
		wordsList = Engine.produceWords(wordDmgMap, 30);
				
		// initialize inputBar
		inputBar = new TextField(gc, gc.getDefaultFont(), gc.getWidth()/3 - 10,
								 gc.getHeight() - 25, 300, 25);
		inputBar.setInput(gameStageInput);
		inputBar.setFocus(true);
		currentText = "";
		
		// initialize player
		player = Resources.createPlayer(Game.playerName);
		
		// initialize opponent
		opponent = Resources.createOpponent("");
		
		gameOver = false;
		lose = false;
		gameOverDBox = new DialogBox("Game Over", "", "", "", "");
		
		// Initialize music and sounds
		bgMusic = new Music("res/musics/Intense_Boss_Music.ogg");
		shootSound = new Sound("res/sounds/8bit_gunloop_explosion.ogg");
		hitSounds = new Sound[5];
		for (int i = 1; i <= hitSounds.length; i++) {
			hitSounds[i - 1] = new Sound("res/sounds/hit_sounds/ogg/hit" + i + ".ogg");
		}
		hitSoundIndex = 0;
		dieSound = new Sound("res/sounds/hit_sounds/ogg/die1.ogg");;
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		// render background
		background.draw(0,0);
		
		// render the configuration icon
		configIcon.draw(0, 600 - configIcon.getHeight());
		
		// render words
		g.setColor(Color.black);
		for (Word word: wordsList) {
			g.drawString(word.name, word.minX, word.minY + 100);
		}
		
		// render inputBar
		g.setColor(Color.white);
		inputBar.render(gc, g);
		
		// render player and opponent
		renderPlayer(player, false);
		renderPlayer(opponent, true);
		
		// Draw player's health bar
		g.setColor(Color.black);
		g.drawString("HP", 25, 40);
		g.fill(player.healthBar, new GradientFill(0, 0, Color.yellow, 0,100, Color.red) );
		g.drawString((int)player.HP + "/" + (int)player.maxHP, player.healthBar.getMinX(), player.healthBar.getMaxY() + 10);
		
		// Draw opponent's health bar
		g.drawString("HP", Game.width - 40, 40);
		g.fill(opponent.healthBar, new GradientFill(0, 0, Color.yellow, 0,100, Color.red) );
		g.drawString((int)opponent.HP + "/" + (int)opponent.maxHP, opponent.healthBar.getMaxX() - 60, opponent.healthBar.getMaxY() + 10);
		
		// Show menu
		if(showMenu){
			// Stop the words from moving
			for (Word word: wordsList) {
				word.stopMovement();
			}
			configDBox.draw(g);
			
			// If resume button is clicked
			if (configDBox.isButton1Clicked()) {
				showMenu = false;
				Random rand = new Random(System.nanoTime());
				for (Word word: wordsList)
					word.startMovement(Movable.Direction.RIGHT, rand.nextFloat() * 0.2f);
			} else if (configDBox.isButton2Clicked()) {
				// if disconnect button is clicked
				showMenu = false;
				gInfo = null;
				sbg.enterState(Game.MENU);
			}
		}
		
		// If the game is over, stop game network, and back to Menu state
		if (gameOver) {
			LAN.stopGameNetwork();
			for (Word word: wordsList)
				word.stopMovement();
			if (lose) {
				gameOverDBox.boxDescription = "You have lost.";
				gameOverDBox.button1 = "Okay";
			} else {
				gameOverDBox.boxDescription = "You have won!";
				gameOverDBox.button1 = "Great";
			}
			gameOverDBox.initComponent();
			gameOverDBox.titlePosX = gameOverDBox.xPos + 60;
			gameOverDBox.draw(g);
			if (gameOverDBox.isButton1Clicked()) {
				// Back to menu
				gameOver = false;
				gInfo = null;
				sbg.enterState(Game.MENU);
			}
		}
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int delta) throws SlickException {
		// Play background music
		if(!bgMusic.playing())
			bgMusic.loop();
		
		inputBar.setFocus(true);
		
		// Update GameInfo and damage
		if (!gameOver) {
			updateGInfo();
		}
		
		// Shoot if words match
		if(gameStageInput.isKeyPressed(Input.KEY_SPACE)){
			currentText = inputBar.getText().trim();
			Word[] wordsArray = wordsList.toArray(new Word[wordsList.size()]);
			
			for (Word word: wordsArray) {
				if (currentText.equals(word.name)) {
					// Change player current animation
					player.current = player.shootingPistol;
					
					// Set player's attack damage
					playerDamage += wordDmgMap.get(word.name);
					
					// Remove the word from wordsList
					wordsList.remove(word);

					currentText = ""; // Reset current text
				}
			}
			inputBar.setText(""); // Empty the text bar
		}
		
		// Reset wordsList if empty
		if (wordsList.isEmpty()) {
			wordsList = Engine.produceWords(wordDmgMap, 30);
		}
		
		// Move words if they are moving
		for (Word word: wordsList) {
			if (word.isMoving)
				word.move(word.speed * delta);
			
			// Reset words' locations when they reach the end of screen
			if (word.minX >= gc.getWidth())
				word.minX = 0;
		}
		
		// if the user presses the gear button or escape button
		if (gameStageInput.isKeyDown(Input.KEY_ESCAPE) || (Engine.isClicked(0, 0, 40, 40) && Mouse.isButtonDown(0)))
			showMenu = true;
		
		// Update game result
		if (player.HP <= 0) {
			if (!player.hasDied) {
				if (!dieSound.playing()) {
					dieSound.play();
					player.hasDied = true;
				}
			}
			gameOver = true;
			lose = true;
		}

	}
	
	public void updateGInfo() {
		/*
		 * Update GameInfo
		 */
		if (gInfo != null) {
			// Update it only if the GameInfo is new
			if (gInfo.randomValue != previousRandomValue) {
				gameOver = gInfo.gameOver;
				opponent = gInfo.sPlayer.deserialize(opponent, true);
				float opponentDamage = gInfo.damage;
				player.takeDamage(opponentDamage); // Damage the player
				
				if (opponentDamage > 0) {
					// Play hit sounds
					if (!hitSounds[hitSoundIndex].playing()) {
						hitSounds[hitSoundIndex].play();
						hitSoundIndex++;
						if (hitSoundIndex >= hitSounds.length) {
							hitSoundIndex = 0;
						}
					}
				}
				
				previousRandomValue = gInfo.randomValue;
			}

		}
	}
	
	public void renderPlayer(Player player, boolean isOpponent) {
		/*
		 * Render player's animation state
		 */
		if (player.current == player.shootingPistol){
			// Play shoot sounds
			if (!shootSound.playing())
				shootSound.play();
			player.current.stopAt(player.current.getFrameCount() - 1);
			player.current.draw(player.minX, player.minY);
			if (isOpponent) {
				// Draw shoot effect
				player.shootEffect.draw(player.minX - 15, player.minY + 50);
			} else {
				// Draw shoot effect
				player.shootEffect.draw(player.minX + 70, player.minY + 10);
			}
			if (player.current.isStopped()) {
				player.current.restart();
				player.current = player.shootingIdle;
			}
		}
		else  {
			player.current.draw(player.minX, player.minY);		
		}
	}
	@Override
	public int getID() {
		return id;
	}
}
