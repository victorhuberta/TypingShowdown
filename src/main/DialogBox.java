package main;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.IOException;
import java.io.InputStream;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.Graphics;
import org.newdawn.slick.Image;
import org.newdawn.slick.SlickException;
import org.newdawn.slick.TrueTypeFont;
import org.newdawn.slick.util.ResourceLoader;

public class DialogBox {
	/*
	 * A DialogBox to be used in different scenarios
	 * throughout the game
	 */
	
	public Image boxBackground;
	public String boxTitle;
	public String boxDescription;
	public String button1;
	public String button2;
	public String button3;
	public TrueTypeFont dialogTitle;
	public TrueTypeFont menuButton1;
	public TrueTypeFont menuButton2;
	public TrueTypeFont menuButton3;
	public int xPos;
	public int yPos;
	public int titlePosX;
	public int titlePosY;
	public int descPosX;
	public int descPosY;
	
	private static int centerX =  Game.width / 4 + 40;
	private static int centerY = Game.height / 4;
	
	// for drawing button 1
	public int minXButton1;
	public int minYButton1; 
	public int maxXButton1;
	
	// for the listener button 1
	public int minYButton1_inverse; 
	public int maxYButton1_inverse; 
	
	// for menu listener button 2
	public int minXButton2;
	public int minYButton2;
	public int maxXButton2;

	// for the listener button 2
	public int minYButton2_inverse;
	public int maxYButton2_inverse;
	
	// for menu listener button 3
	public int minXButton3;
	public int minYButton3;
	public int maxXButton3;

	// for the listener button 3
	public int minYButton3_inverse;
	public int maxYButton3_inverse;
	
	public DialogBox(String title, String desc, String button1, String button2, String button3) throws SlickException {
		this(title, desc, button1, button2, button3, centerX, centerY);
	}
	
	public DialogBox(String title, String desc, String button1, String button2, String button3, int xPos, int yPos) throws SlickException {
		
		boxTitle = title;
		boxDescription = desc;
		this.button1 = button1;
		this.button2 = button2;
		this.button3 = button3;
		this.xPos = xPos;
		this.yPos = yPos;
		this.titlePosX = xPos + 110;
		this.titlePosY = yPos + 70;
		this.descPosX = xPos + 80;
		this.descPosY = yPos + 110;
		initComponent();

	
	}
	
	public DialogBox(String title, String button1, String button2, String button3, int xPos, int yPos) throws SlickException {
		this(title, "", button1, button2, button3, xPos, yPos);
	}
	
	public void initComponent() throws SlickException {
		
		boxBackground = new Image("res/images/dialog/background.png");
		
		InputStream inputStream = ResourceLoader.getResourceAsStream("res/fonts/8BitWonder.ttf");
		try {
			Font awtFont = Font.createFont(Font.TRUETYPE_FONT, inputStream);
			awtFont = awtFont.deriveFont(24f);
			dialogTitle = new TrueTypeFont(awtFont, false);
			awtFont = awtFont.deriveFont(12f);
			
			// initialize the button font
			menuButton1 = new TrueTypeFont(awtFont, false);
			menuButton2 = new TrueTypeFont(awtFont, false);
			menuButton3 = new TrueTypeFont(awtFont, false);
			
			// set minX and minY of button 1
			minXButton1 = xPos + 110;
			minYButton1 = yPos + 150;
			
			// set minY for the listener
			minYButton1_inverse = Game.height - minYButton1 - menuButton1.getHeight(button1); 
			
			// set maxX and maxY for button 1
			maxXButton1 = minXButton1 + menuButton1.getWidth(button1);
			maxYButton1_inverse = Game.height - minYButton1;
			
			// set minX and minY of button 2
			minXButton2 = xPos + 110;
			minYButton2 = yPos + 170;
			
			minYButton2_inverse = Game.height - minYButton2 - menuButton2.getHeight(button2);
			
			// set maxX and maxY for button 2
			maxXButton2 = minXButton2 + menuButton2.getWidth(button2);
			maxYButton2_inverse = Game.height - minYButton2;
			
			// set minX and minY of button 3
			minXButton3 = xPos + 110;
			minYButton3 = yPos + 190;
			
			minYButton3_inverse = Game.height - minYButton3 - menuButton3.getHeight(button3);
			
			// set maxX and maxY for button 3
			maxXButton3 = minXButton3 + menuButton3.getWidth(button3);
			maxYButton3_inverse = Game.height - minYButton3;
			
		} catch (FontFormatException e) {
			System.out.println(e.getMessage());
		} catch (IOException e){
			System.out.println(e.getMessage());
		}
		
	}
	
	
	public void draw(Graphics g){
		/*
		 * Draw the DialogBox's components
		 */
		boxBackground.draw(xPos, yPos);
		dialogTitle.drawString(titlePosX, titlePosY, boxTitle);
		g.drawString(boxDescription, descPosX, descPosY);
		menuButton1.drawString(minXButton1 , minYButton1, button1);
		menuButton2.drawString(minXButton1, minYButton2, button2);
		menuButton3.drawString(minXButton3, minYButton3, button3);
		
	}
	
	public boolean isButton1Clicked() {
		return Mouse.isButtonDown(0) && Engine.isClicked(minXButton1, minYButton1_inverse, 
				maxXButton1, maxYButton1_inverse);
	}
	
	public boolean isButton2Clicked() {
		return Mouse.isButtonDown(0) && Engine.isClicked(minXButton2, minYButton2_inverse, 
				maxXButton2, maxYButton2_inverse);
	}
	
	public boolean isButton3Clicked() {
		return Mouse.isButtonDown(0) && Engine.isClicked(minXButton3, minYButton3_inverse, 
				maxXButton3, maxYButton3_inverse);
	}
}
