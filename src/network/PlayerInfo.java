package network;

import java.net.InetAddress;

public class PlayerInfo implements Comparable<PlayerInfo> {
	/*
	 * Store player's information.
	 * Utilized by game's LAN feature
	 */
	public String name;
	public double highScore;
	public InetAddress address;
	public int port;
	
	public PlayerInfo(String name, double highScore, InetAddress address, int port) {
		this.name = name;
		this.highScore = highScore;
		this.address = address;
		this.port = port;
	}
	
	@Override
	public boolean equals(Object obj) {
		// Basic check for equality
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
		    return false;
		
		// Check if objects' addresses are equal
		PlayerInfo other = (PlayerInfo) obj;
		if (address == null) {
	      if (other.address != null)
	        return false;
	    } else if (!address.equals(other.address))
	    	return false;
		
		return true;
	}

	@Override
	public int compareTo(PlayerInfo other) {
		boolean isEqual = equals(other);
		if (isEqual)
			return 0;
		else
			return -1;
	}
	
	@Override
	public String toString() {
		return name + ":" + highScore + ":" + address;
	}
}
