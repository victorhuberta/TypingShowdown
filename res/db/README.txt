To export / dump a database into .sql file:
mysqldump -u appUser --password=appPassword TypingShowdown > db_typingShowdown.sql

To import a database from .sql file:
mysql -u appUser --password=appPassword -h localhost TypingShowdown < db_typingShowdown.sql
