package movables;

import org.newdawn.slick.geom.Rectangle;

import main.Game;

public class SerializablePlayer extends SerializableMovable<Player> {
	
	/*
	 * A Player class which is serializable (can be sent within network)
	 */
	
	private static final long serialVersionUID = 1L;
	public enum PlayerAnimation {
		shootingIdle, shootingPistol, movingRightPistol, movingLeftPistol
	}
	public PlayerAnimation current;
	public Rectangle healthBar;
	public float barWidth;
	public float percentageInBar;
	public float healthLeft;
	public float maxHP;
	
	public SerializablePlayer(Player player) {
		/*
		 * Construct a serializable Player object by manipulating an existing Player object
		 */
		
		super(player);
		
		// Set the corresponding Animation Enums
		if (player.current == player.shootingIdle) {
			this.current = PlayerAnimation.shootingIdle;
		} else if (player.current == player.shootingPistol) {
			this.current = PlayerAnimation.shootingPistol;
		} else if (player.current == player.movingLeftPistol) {
			this.current = PlayerAnimation.movingLeftPistol;
		} else if (player.current == player.movingRightPistol) {
			this.current = PlayerAnimation.movingLeftPistol;
		}
		
		// Set health-related objects
		this.healthBar = player.healthBar;
		this.barWidth = player.barWidth;
		this.percentageInBar = player.percentageInBar;
		this.healthLeft = player.healthLeft;
		this.maxHP = player.maxHP;
	}
	
	public Player deserialize(Player player, boolean isOpponent) {
		/*
		 * Return a new Player object based on this object
		 */
		
		player = super.deserialize(player);
		
		if (player == null)
			return null;
		
		// If it is an opponent Player object, change its minX and minY
		if (isOpponent) {
			float minX = (float) (0.7 * Game.width);
			float minY = (float) (0.62 * Game.height);
			player.setXY(minX, minY);
		}
		
		// Set the corresponding Animation Enums
		if (this.current == PlayerAnimation.shootingIdle) {
			player.current = player.shootingIdle;
		} else if (this.current == PlayerAnimation.shootingPistol) {
			player.current = player.shootingPistol;
		} else if (this.current == PlayerAnimation.movingLeftPistol) {
			player.current = player.movingLeftPistol;
		} else if (this.current == PlayerAnimation.movingLeftPistol) {
			player.current = player.movingRightPistol;
		}
		
		// Set health-related objects
		player.healthBar = this.healthBar;
		// Mirror location of the original healthBar
		player.healthBar.setX(Game.width - 65); 
		player.barWidth = this.barWidth;
		player.percentageInBar = this.percentageInBar;
		player.healthLeft = -this.healthLeft; // Opposite width
		
		// Set width of the healthBar to 0 if there is no healthLeft
		if (healthLeft <= 0) {
			player.healthBar.setWidth(0);
		} else {
			player.healthBar.setWidth(player.healthLeft);
		}
		
		player.maxHP = this.maxHP;
		
		return player;
		
	}
}
