package network;

public class LAN {
	/*
	 * Start both server and client threads
	 */
	public static Thread multicastClientThread = null;
	public static Thread multicastServerThread = null;
	public static Thread networkThread = null;
	
	public static void runMulticastClient() {
		/*
		 * Enable client multicast capability in ViewLAN state
		 */
		MulticastGameServer mgClient = new MulticastGameServer();

		// Stop game network first
		stopGameNetwork();
		
		// Start multicast client
		multicastClientThread = new Thread(mgClient);
		multicastClientThread.start();
	}
	
	public static void runMulticastServer() {
		/*
		 * Enable server multicast capability in ViewLAN state
		 */
		MulticastGameClient mgServer = new MulticastGameClient();
		
		// Stop game network first
		stopGameNetwork();
		
		// Start multicast server
		multicastServerThread = new Thread(mgServer);
		multicastServerThread.start();
	}
	
	
	public static void runGameNetwork(PlayerInfo pInfo) {
		/*
		 * Enable networking capability in PlayLAN state
		 */
		
		if (networkThread != null) {
			// If networkThread is still alive, do nothing
			if (networkThread.isAlive()) {
				return;
			}
		}
		
		GameNetwork gNetwork = new GameNetwork(pInfo);
		
		// Disable multicast capability first
		// stopMulticastClient();
		stopMulticastServer();
		
		// Start the network
		networkThread = new Thread(gNetwork);
		networkThread.start();
	}
	
	public static void stopMulticastClient() {
		if (multicastClientThread != null) {
			multicastClientThread.interrupt();
		}
	}

	public static void stopMulticastServer() {
		if (multicastServerThread != null) {
			multicastServerThread.interrupt();
		}
	}
	
	public static void stopGameNetwork() {
		if (networkThread != null) {
			networkThread.interrupt();
		}
	}
}
