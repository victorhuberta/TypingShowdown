package main;

import org.newdawn.slick.*;
import org.newdawn.slick.gui.TextField;
import org.newdawn.slick.state.*;

import network.LAN;

import org.lwjgl.input.Mouse;

public class Menu extends BasicGameState {
	
	private Image background;
	private Image exitGame;
	private Image singlePlayer;
	private Image multiPlayer;
	private Image scores;
	private DialogBox scoresDBox; // Scores Dialog Box
	private Image configIcon;
	private DialogBox configDBox; // Difficulty Config Dialog Box
	private boolean showDifficulty;
	private boolean showScores;
	private Music bgMusic;
	
	private Input gameStageInput; // To store any user input
	private TextField inputBar; // The main text field where the user types in
	
	public int id;
	
	public Menu(int id) {
		this.id = id;
	}
	

	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		
		background = new Image ("res/images/menu/TypingShowdown.png");
		// Get user input
		gameStageInput = gc.getInput();
				
		// initialize inputBar
		inputBar = new TextField(gc, gc.getDefaultFont(), 0, gc.getHeight() -25, 300, 25);
		inputBar.setInput(gameStageInput);
		inputBar.setText("Player Name");
		
		// Initialize music and images
		exitGame = new Image ("res/images/menu/exit.png");
		singlePlayer = new Image ("res/images/menu/single.png");
		multiPlayer = new Image ("res/images/menu/multiplayer.png");
		scores = new Image("res/images/menu/scores.png");
		scoresDBox = new DialogBox("Scores", "", "OK", "Cancel", "");
		showScores = false;
		bgMusic = new Music("res/musics/menu.ogg");
		
		// initialize configuration icon
		configIcon = new Image("res/images/decoration/configure.png");
		
		// initialize config dialog box
		configDBox = new DialogBox("Difficulty", "How good are you?", "EASY", "HARD", "EXTREME");
		
		// Set highscore from file
		FileProcessor.readHighscore();
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		// initialize background
	 	g.drawImage( background, 0, 0);
	 	
	 	// render the configuration icon
	 	configIcon.draw(0, 0);
	 	
	 	g.setColor(Color.white);
		inputBar.render(gc, g);
		
		g.setColor(Color.black);
		g.drawString("SPACE to save name & TAB to clear", 
					inputBar.getWidth() + 10, inputBar.getY());
	 	
		// Render buttons
	 	singlePlayer.draw(533,168);
	 	multiPlayer.draw(533,271);
	 	scores.draw(500, 376);
	 	exitGame.draw (533,472);
	 	
	 	// Show the scores in a dialog box
	 	if (showScores) {
	 		scoresDBox.titlePosX = scoresDBox.xPos + 90;
	 		scoresDBox.descPosX = scoresDBox.xPos + 110;
	 		scoresDBox.boxDescription = "" + Game.playerHighscore;
	 		scoresDBox.draw(g);
	 	}
	 	
	 	// Choose difficulty in a dialog box
	 	if(showDifficulty){
	 		// Draw dialog
	 		configDBox.titlePosX = configDBox.xPos + 60;
			configDBox.draw(g);
			
			// Determine which difficulty is chosen
			if (configDBox.isButton1Clicked()) {
				showDifficulty = false;
				// Set difficulty of game and write to file
				Game.diff = Game.Difficulty.EASY;
				FileProcessor.writeDifficulty();
			} else if (configDBox.isButton2Clicked()) {
				showDifficulty = false;
				// Set difficulty of game and write to file
				Game.diff = Game.Difficulty.HARD;
				FileProcessor.writeDifficulty();
			} else if (configDBox.isButton3Clicked()) {
				showDifficulty = false;
				// Set difficulty of game and write to file
				Game.diff = Game.Difficulty.EXTREME;
				FileProcessor.writeDifficulty();
			}
		}
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int g) throws SlickException {
		
		// Play background music if it has not been played
		if(!bgMusic.playing())
			bgMusic.loop();
		
		inputBar.setFocus(true);
		
		// Check name's length if SPACE is pressed
		// If not too long, set game's player name
		if (gameStageInput.isKeyPressed(Input.KEY_SPACE)) {
			String name = inputBar.getText().trim();
			if (name.length() > 8) {
				inputBar.setText("Too long! Max: 8 chars");
			} else {
				Game.playerName = name;
			}
		} else if (gameStageInput.isKeyPressed(Input.KEY_TAB)) {
			// Clean the inputBar if TAB is pressed
			inputBar.setText("");
		}
		
		int posX = Mouse.getX();
		int posY = Mouse.getY();
		
		// Single Player button
		if ((posX>533 && posX<673 )&&(posY> 349 && posY< 431)){
			if (Mouse.isButtonDown(0)){
				sbg.getState(Game.PLAY).init(gc, sbg);
				sbg.enterState(Game.PLAY);
			}
		}
		
		// Multi Player button
		if ((posX>533 && posX<673 )&&(posY>246 && posY<327)){
			if (Mouse.isButtonDown(0)){
				// Run both client and server
				LAN.runMulticastClient();
				LAN.runMulticastServer();
				sbg.getState(Game.VIEW_LAN).init(gc, sbg);
				sbg.enterState(Game.VIEW_LAN);
			}
		}
		
		// Exit button
		if ((posX>533 && posX < 673  )&&(posY > 41  && posY <125)){
			if (Mouse.isButtonDown(0)){
				System.exit(0);
			}
		}
		
	 	// Scores button
		if (Mouse.isButtonDown(0) && Engine.isClicked(505, 144, 703, 218)) {
			showScores = true;
		}
		
		// If scoresDBox button 1 or 2 is clicked, dialog box vanishes
		if (scoresDBox.isButton1Clicked() || scoresDBox.isButton2Clicked()) {
 			showScores = false;
 		}
		
		// If the user presses the gear button or escape button, show configDBox
		if (gameStageInput.isKeyDown(Input.KEY_ESCAPE) || (Engine.isClicked(0, 559, 40, 600) && Mouse.isButtonDown(0)))
			showDifficulty = true;

	}

	@Override
	public int getID() {
		return id;
	}
}
