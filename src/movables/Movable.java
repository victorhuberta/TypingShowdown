package movables;

import org.newdawn.slick.*;

public class Movable {
	/*
	 * The base class of all movable objects in the game,
	 * e.g. Player, Zombie, Bullet, etc
	 */

	// A direction enum for the movable objects
	public enum Direction {
		UP, DOWN, LEFT, RIGHT
	}
	
	public String name;
	public float HP, damage;
	public Animation current, idle, movingUp, movingDown, movingLeft, movingRight;
	public float minX, minY, maxX, maxY;
	public boolean isMoving = false;
	public Direction direction = Direction.RIGHT;
	public float speed = 0.0f;
	
	public Movable() {}
	
	public Movable(String name) {
		this.name = name;
	}
	
	/*
	 * Initializes all the animations
	 */
	
	public void setIdle(Image[] images, int[] duration) {
		idle = new Animation(images, duration, true);
	}
	
	public void setMovingUp(Image[] images, int[] duration) {
		movingUp = new Animation(images, duration, true);
	}
	
	public void setMovingDown(Image[] images, int[] duration) {
		movingDown = new Animation(images, duration, true);
	}
	
	public void setMovingLeft(Image[] images, int[] duration) {
		movingLeft = new Animation(images, duration, true);
	}
	
	public void setMovingRight(Image[] images, int[] duration) {
		movingRight = new Animation(images, duration, true);
	}
	
	public void setXY(float minX, float minY) {
		/*
		 * Automatically generated values for maxX and maxY
		 */
		
		this.minX = minX;
		this.minY = minY;
		if (idle != null) {
			this.maxX = minX + idle.getImage(0).getWidth();
			this.maxY = minY + idle.getImage(0).getHeight();
		}
	}
	
	public void setXY(float minX, float minY, float maxX, float maxY) {
		/*
		 * Overloaded method of setXY, manually assign values to all XYs
		 */
		this.minX = minX;
		this.minY = minY;
		this.maxX = maxX;
		this.maxY = maxY;
	}
	
	public void startMovement(Direction direction, float speed) {
		/*
		 * Prepare the movement of object
		 */
		this.isMoving = true;
		this.direction = direction;
		this.speed = speed;
	}
	
	public void stopMovement() {
		/*
		 * Stop movement of object
		 */
		this.isMoving = false;
		this.speed = 0.0f;
	}
	
	public void move(float distance) {
		/*
		 * Move only if isMoving equals to true
		 */
		
		if (!isMoving)
			return;
		
		switch (direction) {
		case UP:
			setXY(minX, minY - distance);
			break;
		case DOWN:
			setXY(minX, minY + distance);
			break;
		case LEFT:
			setXY(minX - distance, minY);
			break;
		case RIGHT:
			setXY(minX + distance, minY);
			break;
		default:
		}
	}
	
	public float getHP(){
		return HP;
	}	
	
	public void setHP(float newHP){
		HP = newHP;
	}
}
