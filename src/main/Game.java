package main;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;

import network.LAN;

public class Game extends StateBasedGame {
	/*
	 * The core class of the game
	 */
	
	public static final String title = "Typing Showdown !";
	public static final int width = 800;
	public static final int height = 600;
	public static final boolean isFullscreen = false;
	public static final int MENU = 0;
	public static final int PLAY = 1;
	public static final int VIEW_LAN = 2;
	public static final int PLAY_LAN = 3;
	public static final int SPLASH = 4;
	
	public static String playerName = "";
	public static float playerHighscore = 0;
	
	// A difficulty enum with different numeric values,
	// the values are used in the calculation of damage
	public enum Difficulty {
		EASY (1), HARD (2), EXTREME (4);
		
		private int numVal;
		
		Difficulty(int numVal) {
			this.numVal = numVal;
		}
		
		public int getNumVal() {
			return numVal;
		}
	}
	
	// Store difficulty set by user
	public static Difficulty diff;
	
	public Game(String name) {
		super(name);
	}

	public static void main(String[] args) {
		
		/*
		 * Stop all networking threads on shutdown to release resources
		 */
		Runtime.getRuntime().addShutdownHook(new Thread()
		{
		    @Override
		    public void run()
		    {
		    	LAN.stopGameNetwork();
		        LAN.stopMulticastClient();
		        LAN.stopMulticastServer();
		    }
		});
		
		// Set IP preference to IPv4
		System.setProperty("java.net.preferIPv4Stack", "true");
		
		AppGameContainer agc;
		
		try {
			agc = new AppGameContainer(new Game(title));
			agc.setDisplayMode(width, height, isFullscreen);
			agc.setShowFPS(false);
			FileProcessor.readDifficulty(); // Set difficulty from file
			agc.start(); // Start the game container
			
		} catch (SlickException se) {
			se.printStackTrace();
		}
	}

	@Override
	public void initStatesList(GameContainer gc) throws SlickException {
		/*
		 * Initialize all states
		 */
		this.addState(new Splash(SPLASH));
		this.addState(new Menu(MENU));
		this.addState(new Play(PLAY));
		this.addState(new ViewLAN(VIEW_LAN));
		this.addState(new PlayLAN(PLAY_LAN));
		this.enterState(SPLASH);
	}

}
