package main;

import java.io.*;

public class FileProcessor {
	/*
	 * A class to handle all file-related manipulations
	 */
	
	public static void writeHighscore() {
		// Write highscore to file
		String fileName = "highscore";
		String highscore = "" + Game.playerHighscore;
		writeToFile(fileName, highscore);
	}
	
	public static void readHighscore() {
		// Read highscore from file
		String fileName = "highscore";
		String highscoreStr = readFromFile(fileName);
		
		try {
			float highscore = Float.parseFloat(highscoreStr);
			Game.playerHighscore = highscore;
		} catch (NumberFormatException e) {
			System.out.println(e.getMessage());
		} catch (NullPointerException e) {
			System.out.println(e.getMessage());
		}	
	}
	
	public static void writeDifficulty() {
		// Write difficulty to file
		String fileName = "difficulty";
		String difficulty = Game.diff.toString();
		writeToFile(fileName, difficulty);
	}
	
	public static void readDifficulty() {
		// Read difficulty from file
		String fileName = "difficulty";
		String difficulty = readFromFile(fileName).trim();
		
		if (difficulty.equals("HARD")) {
			Game.diff = Game.Difficulty.HARD;
		} else if (difficulty.equals("EXTREME")) {
			Game.diff = Game.Difficulty.EXTREME;
		} else {
			Game.diff = Game.Difficulty.EASY;
		}
	}
	
	public static void writeToFile(String fileName, String content) {
		byte[] data = content.getBytes();
		try {
			FileOutputStream fos = new FileOutputStream(fileName);
			fos.write(data);
			fos.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public static String readFromFile(String fileName) {
		String content = "";
			
		byte[] data = new byte[1024];
		try {
			FileInputStream fis = new FileInputStream(fileName);
			
			while (fis.read(data) != -1) {
				content += (new String(data));
			}
			
			fis.close();
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
		
		return content;
	}
}
