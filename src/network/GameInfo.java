package network;

import java.io.Serializable;
import java.util.Random;

import movables.SerializablePlayer;

public class GameInfo implements Serializable {
	/*
	 * An object that has Game-related information
	 * to be sent within the network 
	 */
	
	private static final long serialVersionUID = 1L;
	public long randomValue; // a unique value of the object
	
	public boolean gameOver; // to check whether the game has ended
	public SerializablePlayer sPlayer;
	public float damage; // damage the player sends
	
	public GameInfo(boolean gameOver, SerializablePlayer sPlayer, float damage) {
		Random rand = new Random(System.nanoTime());
		this.randomValue = rand.nextLong();
		this.gameOver = gameOver;
		this.sPlayer = sPlayer;
		this.damage = damage;
	}
}
