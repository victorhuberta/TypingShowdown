package movables;

public class Word extends Movable {
	/*
	 * Word class to be used by PlayLAN state
	 */
	
	public Word() {}
	
	public Word(String name) {
		super(name);
	}
	
	public Word(String name, float minX, float minY) {
		super(name);
		this.minX = minX;
		this.minY = minY;
	}
}