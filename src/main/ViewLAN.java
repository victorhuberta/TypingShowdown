package main;

import java.util.ArrayList;

import org.newdawn.slick.*;
import org.newdawn.slick.state.*;
import org.newdawn.slick.util.ResourceLoader;

import java.awt.Font;
import java.awt.FontFormatException;
import java.io.*;

import network.*;

public class ViewLAN extends BasicGameState {
	
	/* A Game State.
	 * Discover other players in the same local area network,
	 * and display them page by page.
	 * At the same time, notify LAN for this player's presence
	 */
	
	public int id;
	private Image background;
	private Image wanted_poster;
	private DialogBox challengeDBox; // Challenge Dialog Box
	private boolean showChallengeDBox;
	private int playerIndex; // Current chosen player's index
	
	private Music bgMusic;
	
	// RefreshButton enum to store its location
	private enum RefreshButton {
		minX(664), minY(84), maxX(746), maxY(158);
		private int value;
		private RefreshButton(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
	}
	
	// NextPageButton enum to store its location
	private enum NextPageButton {
		minX(434), minY(101), maxX(479), maxY(141);
		private int value;
		private NextPageButton(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
	}
	
	// PrevPageButton enum to store its location
	private enum PrevPageButton {
		minX(308), minY(101), maxX(353), maxY(141);
		private int value;
		private PrevPageButton(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
	}
	
	// BackButton enum to store its location
	private enum BackButton {
		minX(29), minY(541), maxX(134), maxY(576);
		private int value;
		private BackButton(int value) {
			this.value = value;
		}
		
		public int getValue() {
			return value;
		}
	}
	
	private ArrayList<PlayerInfo[]> pInfoPages; // PlayerInfo pages
	private int currentPInfoPage; // Page number
	
	private TrueTypeFont mainFont; // The main font used in this state
	private TrueTypeFont nameFont; // A font specifically used for writing player's name
	
	private int posterX; // X location of the poster
	private int posterY; // Y location of the poster
	private final float posterScale = 0.3f; // poster scale of the actual image
	
	public ViewLAN(int id) {
		this.id = id;
	}

	@Override
	public void init(GameContainer gc, StateBasedGame sbg) throws SlickException {
		// Initialize all static resources
		background = new Image("res/images/viewlan/viewlan_bg.png");
		wanted_poster = new Image("res/images/viewlan/wanted_poster.png");
		InputStream inputStream = ResourceLoader.getResourceAsStream("res/fonts/GeorgiaBold.ttf");
		
		// Create required fonts
		try {
			Font awtFont = Font.createFont(Font.TRUETYPE_FONT, inputStream);
			awtFont = awtFont.deriveFont(20f);
			mainFont = new TrueTypeFont(awtFont, false);
			awtFont = awtFont.deriveFont(24f);
			nameFont = new TrueTypeFont(awtFont, false);
		} catch (FontFormatException e) {}
		catch (IOException e) {}
		
		challengeDBox = new DialogBox("Challenge", "Waiting for another player",
									"Cancel", "", "");
		showChallengeDBox = false;
		
		pInfoPages = new ArrayList<>();
		currentPInfoPage = 0;
		playerIndex = -1;
		
		posterX = 120;
		posterY = 180;
		
		// Initialize music
		bgMusic = new Music("res/musics/view_lan.ogg");
		bgMusic.setVolume(bgMusic.getVolume()*1.2f);
	}

	@Override
	public void render(GameContainer gc, StateBasedGame sbg, Graphics g) throws SlickException {
		background.draw(0f, 0f, Game.width, Game.height);

		// Display player list if PlayerInfo pages ArrayList is not empty
		if (!pInfoPages.isEmpty()) {
			// Get current page
			PlayerInfo[] pInfoPage = pInfoPages.get(currentPInfoPage);
			
			// Initial location of playerInfo
			int pInfoX = posterX + 18;
			int pInfoY = posterY + 90;
			
			// Draw all posters in the page
			for (PlayerInfo pInfo: pInfoPage) {
				if (pInfo != null) {
					// Draw poster with playerInfo
					wanted_poster.draw(posterX, posterY, posterScale);
					nameFont.drawString(pInfoX, pInfoY, pInfo.name, Color.black);
					mainFont.drawString(pInfoX, pInfoY + 80, ((int) pInfo.highScore) + "pts", Color.black);
					posterX += wanted_poster.getScaledCopy(posterScale).getWidth() + 20;
					pInfoX = posterX + 20;
				}
			}
			
			// Reset values of posterX and posterY
			posterX = 120;
			posterY = 180;
			
			// Update page information
			mainFont.drawString(374, 470, (currentPInfoPage + 1) + " / " + pInfoPages.size(), Color.black);
		} else {
			// Update page information
			mainFont.drawString(374, 470, "0 / 0", Color.black);
		}
		
		// Show challenge dialog box
		if (showChallengeDBox) {
			PlayerInfo pInfo = getPInfo();
			if (pInfo != null) {
				// Run game network and wait for gInfo to be not null
				LAN.runGameNetwork(pInfo);
				
				// Draw challengeDBox on screen
				challengeDBox.titlePosX = challengeDBox.xPos + 60;
				challengeDBox.descPosX = challengeDBox.xPos + 50;
				challengeDBox.draw(g);
				
				// If cancel button is pressed
				if (challengeDBox.isButton1Clicked()) {
					showChallengeDBox = false;
					LAN.stopGameNetwork();
				}
				
				// If gInfo is not null, it means the other player
				// is ready
				if (PlayLAN.gInfo != null) {
					LAN.stopMulticastClient();
					moveToPlayLAN(pInfo, gc, sbg);
				}
			}
		}
	}

	@Override
	public void update(GameContainer gc, StateBasedGame sbg, int g) throws SlickException {
		if(!bgMusic.playing())
			bgMusic.loop();
		
		Input input = gc.getInput();
		
		// Check which button is clicked by user
		if (input.isMousePressed(Input.MOUSE_LEFT_BUTTON)) {
			
			if (Engine.isClicked(RefreshButton.minX.getValue(), RefreshButton.minY.getValue(),
						RefreshButton.maxX.getValue(), RefreshButton.maxY.getValue())) {
				
				// Reload the search function
				MulticastGameServer mgClient = new MulticastGameServer();
				mgClient.searchPlayers(true);
				currentPInfoPage = 0;
				
			} else if (Engine.isClicked(NextPageButton.minX.getValue(), NextPageButton.minY.getValue(),
					NextPageButton.maxX.getValue(), NextPageButton.maxY.getValue())) {
				
				// Go to next page
				if (currentPInfoPage < pInfoPages.size() - 1) {
					currentPInfoPage++;
				}
				
			} else if (Engine.isClicked(PrevPageButton.minX.getValue(), PrevPageButton.minY.getValue(),
					PrevPageButton.maxX.getValue(), PrevPageButton.maxY.getValue())) {
				
				// Go to previous page
				if (currentPInfoPage > 0) {
					currentPInfoPage--;
				}
				
			} else if (Engine.isClicked(BackButton.minX.getValue(), BackButton.minY.getValue(),
					BackButton.maxX.getValue(), BackButton.maxY.getValue())) {
				
				// Stop multicast client and server
				LAN.stopMulticastServer();
				LAN.stopMulticastClient();
				
				// Go back to main menu
				sbg.enterState(Game.MENU);
				
			} else {
				/*
				 *  Check whether user clicked on one of the posters
				 */
				
				// Initialize all locations and sizes of the posters
				int scndPosterX = posterX + wanted_poster.getScaledCopy(posterScale).getWidth() + 20;
				int thirdPosterX = scndPosterX + wanted_poster.getScaledCopy(posterScale).getWidth() + 20;
				int scaledPosterWidth = wanted_poster.getScaledCopy(posterScale).getWidth();
				int scaledPosterHeight = wanted_poster.getScaledCopy(posterScale).getHeight();
				int posterMinY = Game.height - (posterY + scaledPosterHeight);
				int posterMaxY = Game.height - posterY;
				
				// Check which poster the user clicked and show challenge dialog box
				if (Engine.isClicked(posterX, posterMinY, 
						posterX + scaledPosterWidth, posterMaxY)) {
					showChallengeDBox = true;
					playerIndex = 0;
				} else if (Engine.isClicked(scndPosterX, posterMinY,
						scndPosterX + scaledPosterWidth, posterMaxY)) {
					showChallengeDBox = true;
					playerIndex = 1;
				} else if (Engine.isClicked(thirdPosterX, posterMinY,
						thirdPosterX + scaledPosterWidth, posterMaxY)) {
					showChallengeDBox = true;
					playerIndex = 2;
				} 
			}
		}
		
		/*
		 * Reset pInfoPages each time
		 */
		pInfoPages.clear();
		PlayerInfo[] otherPlayers = 
				MulticastGameServer.otherPlayers.toArray(new PlayerInfo[MulticastGameServer.otherPlayers.size()]);
		for (int i = 0; i < otherPlayers.length; i += 3) {
			PlayerInfo[] pInfoPage = new PlayerInfo[3];
			try {
				// Three PlayerInfo per page
				pInfoPage[0] = otherPlayers[i];
				pInfoPage[1] = otherPlayers[i+1];
				pInfoPage[2] = otherPlayers[i+2];
			} catch (IndexOutOfBoundsException e) {}
			
			// Add a playerInfo page to pInfoPages
			pInfoPages.add(pInfoPage);
		}
	
	}

	@Override
	public int getID() {
		return id;
	}
	
	public void moveToPlayLAN(PlayerInfo pInfo, GameContainer gc, StateBasedGame sbg) {
		/*
		 * Move to the playLAN state
		 */
		
		// Init and move to playLAN state
		if (pInfo != null) {
			try {
				sbg.getState(Game.PLAY_LAN).init(gc, sbg);
			} catch (SlickException e) {
				System.out.println(e.getMessage());
			}
			sbg.enterState(Game.PLAY_LAN);
		}
	}
	
	public PlayerInfo getPInfo() {
		/*
		 * Get chosen player's info
		 */
		
		if (playerIndex < 0) {
			return null;
		}
		PlayerInfo pInfo = null;
		
		// Determine which playerInfo the user clicked
		if (!pInfoPages.isEmpty()) {
			pInfo = pInfoPages.get(currentPInfoPage)[playerIndex];
		}
		
		return pInfo;
	}
}
