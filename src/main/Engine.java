package main;

import java.util.*;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.*;

import initializations.Resources;
import movables.*;

public class Engine {
	public static ArrayList<Zombie> produceZombies(HashMap<String, Float> wordDmgMap, Game.Difficulty diff, float acceleration) throws SlickException {
		/*
		 * Produce zombies based on the specified amount
		 */
		int amount = 0;
		float speed = 0.0f;
		
		// Determine the speed of zombies based on difficulty and acceleration
		switch (diff) {
		case EASY:
			amount = 30;
			speed = 0.07f + acceleration;
			break;
		case HARD:
			amount = 50;
			speed = 0.1f + acceleration;
			break;
		case EXTREME:
			amount = 80;
			speed = 0.15f + acceleration;
			break;
		default:
		}
		
		// The distance value is to make sure that the zombies spawn at different locations
		float distance = 0;
		
		ArrayList<Zombie> zombies = new ArrayList<Zombie>();
		String[] words = wordDmgMap.keySet().toArray(new String[wordDmgMap.size()]);
		
		// A random value to randomly choose a word from the list
		Random rand = new Random(System.nanoTime());
		
		// Add zombies to the list, and start their movements
		for (int i = 0; i < amount; i++) {
			int index = rand.nextInt(words.length - 1);
			Zombie zombie = Resources.createZombie(words[index]);
			zombie.minX += distance; // different spawn locations
			zombie.startMovement(Movable.Direction.LEFT, speed);
			zombies.add(zombie);
			distance += 150;
		}
		return zombies;
	}
	
	public static Bullet produceBullet(HashMap<String, Float> wordDmgMap, Player player, Zombie zombie) throws SlickException {
		/*
		 * Produce a bullet tied with player and the targeted zombie
		 */
		
		float damage = 0.0f;
		
		if (wordDmgMap.containsKey(zombie.word))
			 damage = wordDmgMap.get(zombie.word);
		
		// Create the bullet and start its movement
		Bullet bullet = Resources.createBullet(player.maxX, player.minY + 28, zombie, damage);
		bullet.startMovement(Movable.Direction.RIGHT, 0.5f);
		
		return bullet;
	}
	
	public static ArrayList<Word> produceWords(HashMap<String, Float> wordDmgMap, int amount) {
		/*
		 * Produce list of words with random locations for playLAN state
		 */
		Random rand = new Random(System.nanoTime());
		ArrayList<Word> wordsList = new ArrayList<>();
		String[] words = wordDmgMap.keySet().toArray(new String[wordDmgMap.size()]);
		
		for (int i = 0; i < amount; i++) {
			int index = rand.nextInt(wordDmgMap.size());
			
			// Create a word with random location and specified boundary
			Word word = new Word(words[index], rand.nextFloat() * Game.width,
					rand.nextFloat() * Game.height / 3);
			word.startMovement(Movable.Direction.RIGHT, rand.nextFloat() * 0.2f);
			
			// Add word to wordsList
			wordsList.add(word);
		}
		
		return wordsList;
	}
	
	public static <T extends Movable> boolean isCollision(T movable1, T movable2) {
		/*
		 * To check if a collision occurs between two movables
		 */
		return (movable1.minX < movable2.maxX) && (movable1.maxX > movable2.minX + 40)
				&& (movable1.minY < movable2.maxY) && (movable1.maxY > movable2.minY);
	}
	
	public static boolean isClicked(int minX, int minY, int maxX, int maxY) {
		/*
		 * To check if user has clicked within the boundary
		 */
		int mouseX = Mouse.getX();
		int mouseY = Mouse.getY();
		return (mouseX > minX && mouseX < maxX) && (mouseY > minY && mouseY < maxY);
	}
	
}
