package network;

import java.io.*;
import java.net.*;

import main.Game;

public class MulticastGameClient implements Runnable {
	/*
	 * Notify the network of this player's presence
	 */
	private DatagramSocket socket;
	private InetAddress group;
	private int port = 9999;
	
	private final long DELAY = 1000;

	@Override
	public void run() {
		/*
		 * Send multi-cast packets with 1 second delay
		 */
		try {
			socket = new DatagramSocket();
			while (true) {
				try {
					byte[] data = new byte[1024];
					
					// Send player's information in a form of String
					String pInfo = Game.playerName + ":" + Game.playerHighscore;
					data = pInfo.getBytes();
					
					group = InetAddress.getByName("239.255.99.99");
					// Send packet to the multi-cast group
					DatagramPacket packet = new DatagramPacket(data, data.length, group, port);
					socket.send(packet);
				
					Thread.sleep((long)(Math.random() * DELAY));
					
				} catch (UnknownHostException e) { 
					System.out.println("ERROR: "+ e.getMessage());
				} catch (IOException e) {
					System.out.println("ERROR: "+ e.getMessage());
				} 
			}
		} catch (InterruptedException e) {
			stop();
		} catch (SocketException e) {
			System.out.println(e.getMessage());
		}
	}
	
	public void stop() {
		if (socket != null) {
			socket.close();
		}
	}
	
}
