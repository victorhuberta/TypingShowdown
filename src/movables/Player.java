package movables;

import org.newdawn.slick.Animation;

import org.newdawn.slick.Image;
import org.newdawn.slick.geom.Rectangle;

public class Player extends Movable {
	/*
	 * This Player class is used in both Play and PlayLAN states
	 */
	
	// Player-specific Animation objects
	public Animation shootingIdle, shootingPistol, movingRightPistol, movingLeftPistol,
					 shootEffect;
	
	// health-related objects and values
	public Rectangle healthBar;
	public float barWidth;
	public float percentageInBar;
	public float healthLeft;
	public float maxHP;
	public boolean hasDied;
	
	public Player() {}
	
	public Player(String name) {
		super(name);
	}
	
	public Player(String name, Rectangle healthBar, float HP){
		super(name);
		this.healthBar = healthBar;
		barWidth = healthBar.getWidth();
		healthLeft = barWidth;
		this.HP = HP;
		maxHP = HP;
		hasDied = false;
	}
	
	public void setShootingIdle(Image[] images, int[] duration) {
		shootingIdle = new Animation(images, duration, true);
	}
	
	public void setShootingPistol(Image[] images, int[] duration) {
		shootingPistol = new Animation(images, duration, true);
	}
	
	public void setMovingRightPistol(Image[] images, int[] duration) {
		movingRightPistol = new Animation(images, duration, true);
	}
	
	public void setMovingLeftPistol(Image[] images, int[] duration) {
		movingLeftPistol = new Animation(images, duration, true);
	}
	
	public void setShootEffect(Image[] images, int[] duration) {
		shootEffect = new Animation(images, duration, true);
	}
	
	
	public void takeDamage(float damage){
		/*
		 * Damage the player and update the corresponding health bar
		 */
		
		if (healthLeft > 0){
			HP -= damage;
			percentageInBar = damage * barWidth / maxHP;
			healthLeft -= percentageInBar;
			healthBar.setWidth(healthLeft);
			
		}
		
		if (healthLeft <= 0) {
			healthBar.setWidth(0);
			HP = 0;
		}

	}
	
}