package movables;

public class Bullet extends Movable {
	/*
	 * A Bullet class for Play state
	 */
	public Zombie targetZombie;
	
	public Bullet() {
		super();
	}
	
	public Bullet(Zombie targetZombie) {
		/*
		 * Every bullet is tied to a target zombie
		 */
		this();
		this.targetZombie = targetZombie;
	}
}
